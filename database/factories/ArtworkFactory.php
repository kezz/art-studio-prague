<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Artwork;
use Faker\Generator as Faker;

$factory->define(Artwork::class, function (Faker $faker) {
    return [
        'slug'=>$faker->uuid,
        'title' => $faker->word,
        'image'=> $faker->randomElement($array = array ('afterglow','Blue-purple-web','boiling-plasma', 'im-guessing', 'My-head-is')) . '.jpg',
        'description'=>$faker->text,
    ];
});
