@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <h1>Contact</h1>
                    <div class="divider"></div>
                    <form action="{{ route('contact.message') }}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="name" type="text" class="form-control" placeholder="Full Name">
                        </div>

                        <div class="input-group mb-3">
                            <input name="email" type="email" class="form-control" placeholder="Email address">
                        </div>

                        <div class="input-group mb-3">
                            <select class="form-control" name="reason">
                                <option selected disabled>Reason for contact</option>
                                <option value="1">Purchase</option>
                                <option value="1">Exhibition</option>
                                <option value="1">Other</option>
                            </select>
                        </div>

                        <div class="input-group mb-3">
                            <textarea rows="10" class="form-control" name="message"
                                      placeholder="Type your message here..."></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-dark">send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
