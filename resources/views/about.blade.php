@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <h1>About</h1>
                    <div class="divider"></div>
                    <p>This artist creates large unique pieces for bigger rooms and spaces. He is also a UK registered architect, thereby clients can be assured that commissioned pieces will proportionally and stylistically suit the environments in which they are to be displayed. Mixed media materials are used, particularly metal leaf to imbue the pieces with an enlivening reflective quality that changes in different light conditions. The works are abstract, intended to evoke multiple interpretations, embracing differing cultural, ethnic, religious, social and other backgrounds.<br><br>

                        The artist has a significant history of working with major corporations and governmental agencies. Corporate colours can be featured in the overall tonal palate. Detailed specifications can be provided concerning installation and lighting of the work.<br><br>

                        Very large works for ease of transportation and installation are modular, comprising of a number of canvases mounted together, this can enables works to be mounted on curved walls. The structure and backing materials of the works may also be configured to provide acoustic absorption to ameliorate unsatisfactory room acoustics.</p>
                </div>
            </div>
        </div>
    </div>

@endsection
