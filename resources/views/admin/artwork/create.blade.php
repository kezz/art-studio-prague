@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="box">
                    <h1>Add artwork</h1>
                    <div class="divider"></div>
                    <form action="{{ route('artwork.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="title" type="text" class="form-control" placeholder="Title">
                        </div>

                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input name="file" type="file" id="file">
                                <label class="custom-file-label" for="file">Choose file</label>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <textarea rows="5" class="form-control" name="description"
                                      placeholder="Type your message here..."></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-dark">add</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
