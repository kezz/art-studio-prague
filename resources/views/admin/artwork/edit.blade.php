@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="box">
                    <h1>Edit artwork</h1>
                    <div class="divider"></div>

                    <form action="{{ route('artwork.update', $artwork) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="title" type="text" class="form-control" placeholder="Title"
                                   value="{{ $artwork->title }}">
                        </div>

                        @if($artwork->image)
                            <div class="mb-3 text-center">
                                <img class="img-fluid" src="{{ asset('../images/'.$artwork->image) }}">
                            </div>
                        @endif

                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input name="file" type="file" id="file">
                                <label class="custom-file-label" for="file">Choose file</label>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <textarea rows="5" class="form-control" name="description"
                                      placeholder="Type your message here...">{{$artwork->description}}</textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-dark">save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
