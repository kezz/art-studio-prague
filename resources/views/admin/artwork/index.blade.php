@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <h1>Artworks</h1>
                    <div class="divider"></div>

                    <ul class="list-unstyled">
                        @foreach($artworks as $artwork)
                            <div class="box mb-5">
                                <li class="media">
                                    <img src="../images/{{$artwork->image}}" width="150" class="mr-3">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-1"><b>{{$artwork->title}}</b></h5>
                                        {{$artwork->description}}
                                    </div>
                                </li>
                                <div class="divider"></div>
                                <div class="text-right">
                                    <a class="btn text-info" href="{{ route('artwork.edit', $artwork) }}">edit</a>
                                    <a class="btn text-danger" href="{{ route('artwork.delete', $artwork) }}">remove</a>
                                </div>
                            </div>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
    </div>
@endsection
