@extends('layouts.admin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="box">
                    <h1>Messages</h1>
                    <div class="divider"></div>

                    @foreach($messages as $message)
                      <div class="box mb-5">
                          <h5><b>{{ $message->name }}</b></h5>
                          <p>{{ $message->email }}</p>
                          <div class="divider"></div>
                          <p>{{ $message->message }}</p>
                          <div class="divider"></div>
                          <div class="text-center">
                              <div class="small">{{ $message->created_at->diffForHumans() }}</div>
                          </div>
                      </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection
