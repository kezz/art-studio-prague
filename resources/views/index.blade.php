@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="grid">
            <div class="grid-sizer"></div>
            @foreach($artworks as $artwork)
                <div class="grid-item">
                    <a href="/artwork/{{$artwork->slug}}">
                        <img class="" src="{{ asset('images/'.$artwork->image) }}">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
