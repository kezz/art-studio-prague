@extends('layouts/app')

@section('content')

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-4">
                <div class="box">
                    <h1>{{ $artwork->title }}</h1>
                    <div class="divider"></div>
                    <p>{{ $artwork->description }}</p>
                </div>
            </div>
            <div class="col-md-8">
                <div class="mb-5">
                    <img class="img-fluid w-100 shaddow-3" src="/images/{{$artwork->image}}">
                </div>
            </div>
        </div>
    </div>



@endsection
