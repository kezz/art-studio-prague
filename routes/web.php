<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index')->name('index');
Route::get('/about', 'AppController@about')->name('about');
Route::get('/contact', 'AppController@contact')->name('contact');
Route::post('/contact', 'AppController@message')->name('contact.message');
Route::get('/artwork/{slug}', 'AppController@artwork')->name('artwork');

Route::middleware(['auth'])->group(function ()
{
    Route::get('admin/dashboard', 'ArtworkController@index')->name('artwork.index');
    Route::get('admin/artwork/create', 'ArtworkController@create')->name('artwork.create');
    Route::get('admin/artwork/messages', 'MessageController@index')->name('messages');
    Route::post('admin/artwork/store', 'ArtworkController@store')->name('artwork.store');

    Route::get('admin/artwork/edit/{id}', 'ArtworkController@edit')->name('artwork.edit');
    Route::post('admin/artwork/update/{id}', 'ArtworkController@update')->name('artwork.update');
    Route::get('admin/artwork/delete/{id}', 'ArtworkController@destroy')->name('artwork.delete');

});

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
