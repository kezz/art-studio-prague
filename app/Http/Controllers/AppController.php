<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Artwork;
use App\Message;
use Illuminate\Http\Request;

class AppController extends Controller
{
    private $artwork;
    private $message;

    public function __construct(Artwork $artwork, Message $message)
    {
        $this->artwork = $artwork;
        $this->message = $message;
    }

    public function index()
    {
        $artworks = $this->artwork->all();
        return view('index', ['artworks' => $artworks]);
    }

    public function artwork(string $slug)
    {
        $artwork = $this->artwork->where('slug', $slug)->first();
        return view('artwork', ['artwork' => $artwork]);
    }

    public function contact()
    {
        return view('contact');
    }

    public function message(Request $request)
    {
        $this->message->create($request->all());

        return redirect()->back()->with(
            'message',
            'Thank you for contact ' . config('app.name') . ' you\'re message has been sent'
        );
    }

    public function about()
    {
        return view('about');
    }
}
