<?php

declare(strict_types=1);

namespace App\Http\Controllers\Services;

class ServiceHelper
{
    public function GenerateHash(int $int)
    {
        $characters = 'abcdefghijklmopqrstuvxwyz0123456789';
        $lower = strlen($characters);
        $lower--;

        $hash = null;

        for ($x = 1; $x <= $int; $x++) {
            $Posicao = random_int(0, $lower);
            $hash .= substr($characters, $Posicao, 1);
        }

        return $hash;
    }
}
