<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Artwork;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    private $artwork;

    /**
     * Create a new controller instance.
     *
     * @param $artwork
     */
    public function __construct(Artwork $artwork)
    {
        $this->middleware('auth');
        $this->artwork = $artwork;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $artworks = $this->artwork->all();
        return view('home', ['artworks' => $artworks]);
    }
}
