<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Artwork;
use App\Http\Controllers\Services\ServiceHelper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ArtworkController extends Controller
{
    private $helper;

    private $artwork;

    public function __construct(Artwork $artwork, ServiceHelper $serviceHelper)
    {
        $this->artwork = $artwork;
        $this->helper = $serviceHelper;
    }

    public function index()
    {
        $artworks = $this->artwork->all();
        return view('admin.artwork.index', ['artworks' => $artworks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.artwork.create');
    }

    public function store(Request $request)
    {
        $file = $request->file('file');
        $fileName = $this->helper->GenerateHash(25);
        $file->storeAs('images', $fileName . '.' . $file->getClientOriginalExtension());

        $this->artwork->create([
            'title' => $request->input('title'),
            'slug' => $this->helper->GenerateHash(15),
            'image' => $fileName . '.' . $file->getClientOriginalExtension(),
            'description' => $request->input('description'),
        ]);

        return redirect()->back()->with('message', 'Artwork added successful');
    }

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show(Artwork $artwork)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(int $id)
    {
        $artwork = $this->artwork->findOrFail($id);
        return view('admin.artwork.edit', ['artwork' => $artwork]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update(Request $request, int $id)
    {
        $artwork = $this->artwork->findOrFail($id);

        $file = $request->file('file');
        if ($file) {
            $fileName = $this->helper->GenerateHash(25);
            $file->storeAs('images', $fileName . '.' . $file->getClientOriginalExtension());
        }

        $artwork->update([
            'title' => $request->input('title'),
            'slug' => $this->helper->GenerateHash(15),
            'image' => $file ? $fileName . '.' . $file->getClientOriginalExtension() : $artwork->image,
            'description' => $request->input('description'),
        ]);

        return redirect()->back()->with('message', 'Artwork updated successful');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $artwork = $this->artwork->findOrFail($id);
        $artwork->delete();
        return redirect()->back()->with('message', 'Artwork deleted');
    }
}
