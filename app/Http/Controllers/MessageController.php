<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    private $message;

    /**
     *
     * @param $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function index(){
        $messages = $this->message->all();
        return view('admin.artwork.messages', ['messages'=>$messages]);
    }
}
